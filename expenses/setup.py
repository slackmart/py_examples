#!/usr/bin/env python3
from setuptools import setup

setup(
    name='expenses',
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
