from datetime import date
from csv import DictWriter, DictReader


class ExpenseManager(object):
    """Initialize self._expenses dictionary with data from file"""
    def __init__(self, file_name, field_names=None):
        self.file_name = file_name
        self._expenses = self.extract_expenses(self.file_name)
        # self.field_names = field_names

    def list(self):
        """Return current expenses data."""
        return self._expenses

    def extract_expenses(self, file_name=None):
        """Extract items from file."""
        file_name = file_name or self.file_name
        with open(file_name, 'r') as storage_file:
            dict_reader = DictReader(storage_file)
            self.field_names = dict_reader.fieldnames
            return {item['date']: item for item in dict_reader}

    def add(self, item):
        """Add item to self._expenses dict using current date.

        It'll update the self._expenses dict using the current date (if the
        record exist, otherwise it'll create a new item with the object
        specified.
        """
        today = date.today()
        actual_date = f'{today.year}-{str(today.month).zfill(2)}-' \
                      f'{str(today.day).zfill(2)}'
        initial_data = {'date': actual_date, 'total': 0}
        today_expenses = self._expenses.get(actual_date, initial_data)
        for concept, amount in item.items():
            current_amount = int(today_expenses.get(concept, 0))
            current_total = int(today_expenses.get('total', 0))

            today_expenses[concept] = current_amount + amount
            today_expenses['total'] = current_total + amount

        self._expenses.update({actual_date: today_expenses})

    def save(self):
        """Save self._expenses to file."""
        with open(self.file_name, 'w') as storage_file:
            expenses_writer = DictWriter(storage_file,
                                         fieldnames=self.field_names)
            expenses_writer.writeheader()
            expenses_writer.writerows(
                [value for key, value in self._expenses.items()])
