from datetime import date
from unittest import TestCase, skip
from unittest.mock import patch
from pytest import raises
from csv import DictWriter
from io import StringIO
from itertools import repeat

from expenses import ExpenseManager


class ExpenserTestCase(TestCase):
    def _parse_initial_data(self, raw_data):
        raw_data_list = raw_data.split('\n')
        self.headers = raw_data_list[0].split(',')

        def f(headers, item):
            return dict(x for x in zip(headers, item))

        headers_xn = [x for x in repeat(self.headers, len(raw_data_list) -1)]
        data = [x.split(',') for x in raw_data_list[1:]]

        return {
            item['date']: item for item in map(f, headers_xn, data)
        }

    def setUp(self):
        self.file_name = 'sample.csv'
        raw_data = 'date,food,transportation,total\n2017-08-31,34,24,58\n' \
                   '2017-08-01,30,0,30'
        self.initial_data = self._parse_initial_data(raw_data)
        with patch('expenses.expense_manager.open') as mock_open:
            mock_open.return_value = StringIO(raw_data)
            self.expense_manager = ExpenseManager(self.file_name,
                                                  field_names=self.headers)

        self.item_to_add = {'transportation': 24, 'food': 4}

        today = date.today()
        self.actual_date = f'{today.year}-{str(today.month).zfill(2)}-' \
                           f'{str(today.day).zfill(2)}'

    def test_add_expense(self):
        self.expense_manager.add(self.item_to_add)

        current_expenses = self.expense_manager.list()
        expected = {
            'date': self.actual_date,
            'total': sum([v for v in self.item_to_add.values()])
        }
        expected.update(self.item_to_add)
        assert expected == current_expenses[self.actual_date]

    @patch.object(ExpenseManager, 'extract_expenses')
    def test_expense_manager_calls_extract_expenses(self, extract_expenses):
        ExpenseManager(self.file_name, self.headers)
        extract_expenses.assert_called_once_with(self.file_name)


    def test_list_raises_keyerror_if_no_actual_date_items(self):
        with raises(KeyError):
            self.expense_manager.list()[self.actual_date]

    def test_add_item_uses_current_date(self):
        self.expense_manager.add(self.item_to_add)

        current_expenses = self.expense_manager.list()
        expected = self.item_to_add
        expected.update({
            'date': self.actual_date,
            'total': sum([v for v in self.item_to_add.values()])
        })
        assert current_expenses[self.actual_date] == expected

    def test_add_expenses_single_record_for_each_day(self):
        self.expense_manager.add(self.item_to_add)
        self.expense_manager.add(self.item_to_add)
        assert len(self.expense_manager.list()) == 3

    @patch('expenses.expense_manager.open')
    def test_save_expenses_uses_open_built_in(self, open_mock):
        self.expense_manager.save()
        open_mock.assert_called_once_with(self.file_name, 'w')

    @patch.object(DictWriter, 'writerows')
    @patch.object(DictWriter, 'writeheader')
    def test_save_expenses_uses_dictwriter(self, dictwriter_writeheader_mock,
                                           dictwriter_writerows_mock):
        self.expense_manager.save()
        dictwriter_writeheader_mock.assert_called_once()
        dictwriter_writerows_mock.assert_called_once_with(
                [value for key, value in self.initial_data.items()])

    @patch('expenses.expense_manager.open')
    @patch('expenses.expense_manager.DictReader')
    def test_extract_expenses_uses_dictreader(self, dictreader_mock, omock):
        self.expense_manager.extract_expenses(self.file_name)
        dictreader_mock.assert_called_once()

    def test_accept_empty_field_names_and_extract_them(self):
        with patch('expenses.expense_manager.open') as mock_open:
            mock_open.return_value = StringIO('date,food,transportation,total\n2017-08-31,4,2,6')
            manager = ExpenseManager('sample.csv')
            assert manager.field_names == self.headers
