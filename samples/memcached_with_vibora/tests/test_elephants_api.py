from vibora.tests import TestSuite

from ..elephants import app


async def create_elephants(cli, uri='/elephants', names=None):
    for name in names or ():
        await cli.post(uri, {'name': name})


class HomeTestCase(TestSuite):
    def setUp(self):
        self.client = app.test_client()

    async def test_health(self):
        response = await self.client.get('/health')

        assert response.status_code == 200
        js_resp = response.json()
        assert js_resp == {'version': '1.0.0'}, js_resp

    async def test_create_elephant(self):
        response = await self.client.post('/elephants', json={'name': 'dumbo'})

        assert response.status_code == 201, response.status_code

        js_resp = response.json()
        assert js_resp['name'] == 'dumbo', js_resp

    async def test_get_elephants_no_elephants(self):
        response = await self.client.get('/elephants')
        assert response.json() == {'elephants': []}, response.text()

    async def test_get_elephants_two_elephants(self):
        create_elephants(self.client, names=('dumbo', 'eli'))
        response = await self.client.get('/elephants')
        js_resp = response.json()
        assert len(js_resp['elephants']) == 2, js_resp

    async def test_post_duplicated_elephant(self):
        name = 'dumbo'
        response = await self.client.get('/elephants')
        already_exists = name in response.json()['elephants']

        response = await self.client.post('/elephants', json={'name': name})

        assert response.status_code == 201, response.status_code
        assert response.json()['created'] != already_exists
