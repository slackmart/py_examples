from vibora import Vibora, Request
from vibora.responses import JsonResponse
from vibora.router import RouterStrategy
from pymemcache.client.base import Client


app = Vibora(router_strategy=RouterStrategy.STRICT)
mem_cli = Client(('localhost', 11200))
resource_name = 'elephants'


@app.route('/health')
async def health():
    return JsonResponse({'version': '1.0.0'})


@app.route('/elephants', methods=['GET', 'POST'])
async def elephants(request: Request):
    elephants = mem_cli.get(resource_name, default=b'').decode()
    if request.method == b'POST':
        data = await request.json()
        name = data['name'].lower()
        if not elephants:
            mem_cli.set(resource_name, name)
        elif name not in elephants:
            mem_cli.append(resource_name,  ',' + name)
        response_payload = {'name': name, 'created': name not in elephants}
        return JsonResponse(response_payload, status_code=201)
    response_payload = {
        resource_name: elephants.split(',') if elephants else []
    }
    return JsonResponse(response_payload)


if __name__ == '__main__':
    app.run(debug=True)
